<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $email = $_POST['femail'];
        $pwd = $_POST['fpwd'];

        if (empty($email)) {
            echo "Email is empty";
        } 
        elseif (empty($pwd)) {
            echo "Password is empty";
        }
        else {

            $userId = CheckLogin($email, $pwd);

            if ($userId != null) {
                setcookie('gochistore_userid', $userId, time() + 7200, "/"); // 7200 = 2h
                header("Location: ../index.php"); 
            }
            else{
                echo "Login failed.";
            }
        }
    }

    function CheckLogin($email, $plaintext_password){
        require 'config.php';
        require '../vendor/autoload.php';

        $connection = new MongoDB\Client('mongodb://localhost:27017');
        $collection = $connection->{$db}->StoreUsers;
            
        $user = $collection->findOne(['email'=> $email]);

        if ($user != null) {

            $verify = password_verify($plaintext_password, $user['password_hash']);

            if ($verify) {
                return $user['_id'];
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }
?>