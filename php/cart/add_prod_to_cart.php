<?php
    require '../../vendor/autoload.php';
    require '../config.php';

    $userId = $_POST['userId'];
    $itemId = $_POST['itemId'];

    $connection = new MongoDB\Client('mongodb://localhost:27017');
    $cartCollection = $connection->{$db}->UserCart;

    $userCart = $cartCollection->findOne(['user_id' => $userId]);

    if ($userCart != null) {
        $prodFound = false;
        $prodIndex = 0;
        $prodQuantity = 0;
        foreach ($userCart->products as $key => $value) {
            if ($value['item_id'] == $itemId) {
                $prodFound = true;
                $prodQuantity = $value['quantity'];
                break;
            }
            $prodIndex++;
        }
        if ($prodFound == true) {
            $cartCollection->updateOne(
                ['_id' => $userCart['_id']],
                ['$set' => ['products.' . $prodIndex . '.quantity' => $prodQuantity + 1]]
            );
        }
        else {
            $new_prod = array(
                "item_id" => $itemId,
                "quantity" => 1
            );
            $cartCollection->updateOne(
                ['_id' => $userCart['_id']],
                ['$push' => ['products' => $new_prod]]
            );
        }
    }         
?>
