<?php
    require 'config.php';
    require '../vendor/autoload.php';
    require 'signup_confirm_mail.php';

    $username = $_POST['uname'];
    $email = $_POST['email'];
    $pwd = $_POST['pwd'];
    $dusage = $_POST['dusage'];
    $gochisam = $_POST['gochisam'];
    $recaptcha = $_POST['g-recaptcha-response'];
    $res = reCaptcha($recaptcha);
    
    $connection = new MongoDB\Client('mongodb://localhost:27017');
    $usersCollection = $connection->{$db}->StoreUsers;
    $mailauthCollection = $connection->{$db}->MailAuth;
    $user = $usersCollection->findOne(['email' => $email]);

    if ($user == null && $res['success']) {
        $authCode = sprintf("%06d", mt_rand(1, 999999));
        $mailAuth = array(
            "username" => $username,
            "email" => $email,
            "authCode" => $authCode,
            "pwd_hash" => password_hash($pwd, PASSWORD_DEFAULT),
            "timeout" => time() + 180
        );
        $mailauthCollection->insertOne($mailAuth);
        SendConfirmEmail($email, $authCode);
        header("Location: ../verification.php?email=" . $email); 
    }


    function reCaptcha($recaptcha){
        $secret = "6LdV9T4aAAAAAIiwS9MpkxjjCHl9a32_Z47nsCx8";
        $ip = $_SERVER['REMOTE_ADDR'];
      
        $postvars = array("secret"=>$secret, "response"=>$recaptcha, "remoteip"=>$ip);
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        $data = curl_exec($ch);
        curl_close($ch);
      
        return json_decode($data, true);
      }
?>