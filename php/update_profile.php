<?php
    require 'config.php';
    require '../vendor/autoload.php';

    $first_name = $_POST['fname'];
    $last_name = $_POST['lname'];
    $birthdate = $_POST['bdate'];
    $email = $_POST['email'];
    $sex = $_POST['sex'];
    $country = $_POST['country'];
    $city = $_POST['city'];
    $newPwd = $_POST['newPwd'];
    
    $connection = new MongoDB\Client('mongodb://localhost:27017');
    $usersCollection = $connection->{$db}->StoreUsers;
    $user = $usersCollection->findOne(['_id' => new MongoDB\BSON\ObjectId($_COOKIE['gochistore_userid'])]);

    if ($user != null) {
        $usersCollection->updateOne(
            ['_id' => $user['_id']],
            ['$set' => ['first_name' => $first_name, 'last_name' => $last_name, 'birth' => $birthdate, 'email' => $email, 'sex' => $sex, 'country' => $country, 'city' => $city]]
        );
        if ($newPwd != "") {
            $usersCollection->updateOne(
                ['_id' => $user['_id']],
                ['$set' => ['password_hash' => password_hash($newPwd, PASSWORD_DEFAULT)]]
            );
        }
        header("Location: ../profile.php"); 
    }
?>