<?php
    // Import PHPMailer classes into the global namespace
    // These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    // Load Composer's autoloader
    require '../vendor/autoload.php';
    require 'config.php';

    $email = $_POST['email'];

    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);
    $randomPwd = randomPassword();

    $connection = new MongoDB\Client('mongodb://localhost:27017');
    $usersCollection = $connection->{$db}->StoreUsers;
    $user = $usersCollection->findOne(['email' => $email]);

    if ($user != null) {
        $usersCollection->updateOne(
            ['_id' => $user['_id']],
            ['$set' => ['password_hash' => password_hash($randomPwd, PASSWORD_DEFAULT)]]
        );
    }

    try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'storegochi@gmail.com';                     // SMTP username
        $mail->Password   = 'Patata123';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom('storegochi@gmail.com', 'Gochi-Store');
        $mail->addAddress($email);

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'New Password - Gochi-Store';
        $mail->Body    = 'Your new password is <b>' . $randomPwd . '</b>';
        $mail->AltBody = 'Your new password is ' . $randomPwd;

        $mail->send();
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

?>