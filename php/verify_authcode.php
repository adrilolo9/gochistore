<?php
    require 'config.php';
    require '../vendor/autoload.php';

    $email = $_POST['email'];
    $code = $_POST['authCode'];

    $connection = new MongoDB\Client('mongodb://localhost:27017');
    $mailauthCollection = $connection->{$db}->MailAuth;
    $userCollection = $connection->{$db}->StoreUsers;
    $userCartCollection = $connection->{$db}->UserCart;

    $mailAuth = $mailauthCollection->findOne(['email'=> $email, 'timeout' => ['$gte' => time()]]);

    if($mailAuth != null){
        if ($mailAuth['authCode'] == $code) {
            $newUser = array(
                "username" => $mailAuth['username'],
                "email" => $email,
                "password_hash" => $mailAuth['pwd_hash'],
                "first_name" => "",
                "last_name" => "",
                "birth" => "",
                "sex" => "",
                "country" => "",
                "city" => ""
            );
            $userCollection->insertOne($newUser);
            $newUserMongo = $userCollection->findOne(['email'=> $email]);

            $newCart = array(
                "user_id" => (string)$newUserMongo['_id'],
                "products" => [],
            );
            $userCartCollection->insertOne($newCart);

            setcookie('gochistore_userid', $newUserMongo['_id'], time() + 7200, "/"); // 7200 = 2h
        }
    }
?>