<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gochi-Store</title>
    <meta name="description" content="Gochi-Store">
    <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <!--Replace with your tailwind.css once created-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:200,400&display=swap" rel="stylesheet">

    <style>
        .worksans {
            font-family: 'Work Sans', sans-serif;
        }

        #menu-toggle:checked+#menu {
            display: block;
        }

        .hover\:grow {
            transition: all 0.3s;
            transform: scale(1);
        }

        .hover\:grow:hover {
            transform: scale(1.02);
        }

        .carousel-open:checked+.carousel-item {
            position: static;
            opacity: 100;
        }

        .carousel-item {
            -webkit-transition: opacity 0.6s ease-out;
            transition: opacity 0.6s ease-out;
        }

        #carousel-1:checked~.control-1,
        #carousel-2:checked~.control-2,
        #carousel-3:checked~.control-3 {
            display: block;
        }

        .carousel-indicators {
            list-style: none;
            margin: 0;
            padding: 0;
            position: absolute;
            bottom: 2%;
            left: 0;
            right: 0;
            text-align: center;
            z-index: 10;
        }

        #carousel-1:checked~.control-1~.carousel-indicators li:nth-child(1) .carousel-bullet,
        #carousel-2:checked~.control-2~.carousel-indicators li:nth-child(2) .carousel-bullet,
        #carousel-3:checked~.control-3~.carousel-indicators li:nth-child(3) .carousel-bullet {
            color: rgb(255, 255, 255);
            /*Set to match the Tailwind colour you want the active one to be */
        }
    </style>

</head>

<body class="bg-white text-gray-600 work-sans leading-normal text-base tracking-normal">

    <!--Nav-->
    <nav id="header" class="w-full z-30 top-0 py-1">
        <div class="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-6 py-3">

            <label for="menu-toggle" class="cursor-pointer md:hidden block">
                <svg class="fill-current text-gray-900" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                    viewBox="0 0 20 20">
                    <title>menu</title>
                    <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                </svg>
            </label>
            <input class="hidden" type="checkbox" id="menu-toggle" />

            <div class="hidden md:flex md:items-center md:w-auto w-full order-3 md:order-1" id="menu">
            </div>

            <div class="order-1 md:order-2">
                <a class="flex items-center tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl "
                    href="index.php">
                    <svg height="40" viewBox="0 0 128 128" width="40" xmlns="http://www.w3.org/2000/svg">
                        <g>
                            <path
                                d="m70 17.392a7.75 7.75 0 1 0 -12.008 0c-22.56 4.681-39.323 34.97-39.323 61.34 0 27.043 17.792 44.518 45.327 44.518s45.327-17.475 45.327-44.518c.004-26.37-16.759-56.659-39.323-61.34zm-10.25-4.892a4.25 4.25 0 1 1 4.25 4.25 4.255 4.255 0 0 1 -4.25-4.25zm4.25 7.75c20.124 0 36.38 22.945 40.693 46.493l-7.562-3.649a1.754 1.754 0 0 0 -1.8.169l-3.745 2.768-4-.811v-18.176a1.749 1.749 0 0 0 -1.75-1.75h-43.669a1.749 1.749 0 0 0 -1.75 1.75v18.176l-4 .811-3.755-2.769a1.75 1.75 0 0 0 -1.8-.168l-7.551 3.64c4.313-23.545 20.567-46.484 40.689-46.484zm-20.083 46.444v-17.9h40.166v31.745h-40.166zm20.083 53.056c-20.221 0-41.827-10.775-41.827-41.018a64.618 64.618 0 0 1 .486-7.8l8.771-4.232 3.531 2.605a1.746 1.746 0 0 0 1.387.306l4.069-.824v13.5a1.749 1.749 0 0 0 1.75 1.75h43.666a1.749 1.749 0 0 0 1.75-1.75v-13.5l4.069.824a1.738 1.738 0 0 0 1.388-.307l3.521-2.6 8.782 4.238a64.637 64.637 0 0 1 .484 7.789c0 30.244-21.606 41.019-41.827 41.019z" />
                            <path
                                d="m101.577 76.982a1.751 1.751 0 0 0 -1.75 1.75 43.212 43.212 0 0 1 -1.827 12.935 1.748 1.748 0 0 0 1.144 2.193 1.721 1.721 0 0 0 .526.081 1.747 1.747 0 0 0 1.668-1.225 46.651 46.651 0 0 0 1.992-13.984 1.751 1.751 0 0 0 -1.753-1.75z" />
                            <path
                                d="m48.625 95.083a4.75 4.75 0 1 0 4.75 4.75 4.756 4.756 0 0 0 -4.75-4.75zm0 6a1.25 1.25 0 1 1 1.25-1.25 1.252 1.252 0 0 1 -1.25 1.25z" />
                            <path
                                d="m64 100.5a4.75 4.75 0 1 0 4.75 4.75 4.756 4.756 0 0 0 -4.75-4.75zm0 6a1.25 1.25 0 1 1 1.25-1.25 1.252 1.252 0 0 1 -1.25 1.25z" />
                            <path
                                d="m79.375 95.083a4.75 4.75 0 1 0 4.75 4.75 4.756 4.756 0 0 0 -4.75-4.75zm0 6a1.25 1.25 0 1 1 1.25-1.25 1.252 1.252 0 0 1 -1.25 1.25z" />
                        </g>
                    </svg>
                    GOCHI-STORE
                </a>
            </div>

            <div class="order-2 md:order-3 flex items-center" id="nav-content">

            <?php
                if(isset($_COOKIE['gochistore_userid'])) {
                    echo '<a class="inline-block no-underline hover:text-black" href="profile.php">
                            <svg class="fill-current hover:text-black" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                viewBox="0 0 24 24">
                                <circle fill="none" cx="12" cy="7" r="3" />
                                <path
                                    d="M12 2C9.243 2 7 4.243 7 7s2.243 5 5 5 5-2.243 5-5S14.757 2 12 2zM12 10c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3S13.654 10 12 10zM21 21v-1c0-3.859-3.141-7-7-7h-4c-3.86 0-7 3.141-7 7v1h2v-1c0-2.757 2.243-5 5-5h4c2.757 0 5 2.243 5 5v1H21z" />
                            </svg>
                        </a>

                        <a class="pl-3 inline-block no-underline hover:text-black" href="cart.php">
                            <svg class="fill-current hover:text-black" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                viewBox="0 0 24 24">
                                <path
                                    d="M21,7H7.462L5.91,3.586C5.748,3.229,5.392,3,5,3H2v2h2.356L9.09,15.414C9.252,15.771,9.608,16,10,16h8 c0.4,0,0.762-0.238,0.919-0.606l3-7c0.133-0.309,0.101-0.663-0.084-0.944C21.649,7.169,21.336,7,21,7z M17.341,14h-6.697L8.371,9 h11.112L17.341,14z" />
                                <circle cx="10.5" cy="18.5" r="1.5" />
                                <circle cx="17.5" cy="18.5" r="1.5" />
                            </svg>
                        </a>
                        
                        <a class="pl-3 inline-block no-underline hover:text-black" onclick="logout()">
                            <svg class="fill-current hover:text-black" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="24" height="24">
                                <path d="M 24.96875 1 C 11.73475 1 0.96875 11.766 0.96875 25 C 0.96875 38.233 11.73475 49 24.96875 49 C 34.39175 49 42.97575 43.4275 46.84375 34.8125 C 47.29675 33.8055 46.85175 32.6395 45.84375 32.1875 C 44.83675 31.7325 43.6405 32.1795 43.1875 33.1875 C 39.9635 40.3665 32.81875 45 24.96875 45 C 13.94075 45 4.96875 36.028 4.96875 25 C 4.96875 13.972 13.94075 5 24.96875 5 C 32.81975 5 39.9635 9.6315 43.1875 16.8125 C 43.6395 17.8205 44.83875 18.2625 45.84375 17.8125 C 46.85175 17.3595 47.29675 16.1955 46.84375 15.1875 C 42.97475 6.5725 34.39175 1 24.96875 1 z M 24 16 C 23.488125 16 22.98375 16.20325 22.59375 16.59375 L 15.59375 23.59375 C 14.81275 24.37475 14.81275 25.62525 15.59375 26.40625 L 22.59375 33.40625 C 22.98375 33.79725 23.488 34 24 34 C 24.512 34 25.01525 33.79725 25.40625 33.40625 C 26.18725 32.62525 26.18725 31.37475 25.40625 30.59375 L 21.8125 27 L 47 27 C 48.104 27 49 26.104 49 25 C 49 23.896 48.104 23 47 23 L 21.8125 23 L 25.40625 19.40625 C 26.18725 18.62525 26.18725 17.37475 25.40625 16.59375 C 25.01575 16.20325 24.511875 16 24 16 z"/>
                            </svg>
                        </a>';
                } else {
                    echo '<a class="inline-block no-underline hover:text-black" href="login.php">
                            <svg class="fill-current hover:text-black" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="24" height="24">
                                <path d="M 25 1 C 15.577 1 6.993 6.5725 3.125 15.1875 C 2.672 16.1945 3.117 17.3605 4.125 17.8125 C 5.132 18.2675 6.32825 17.8205 6.78125 16.8125 C 10.00525 9.6335 17.15 5 25 5 C 36.028 5 45 13.972 45 25 C 45 36.028 36.028 45 25 45 C 17.149 45 10.00525 40.3685 6.78125 33.1875 C 6.32925 32.1795 5.13 31.7375 4.125 32.1875 C 3.117 32.6405 2.672 33.8045 3.125 34.8125 C 6.994 43.4275 15.577 49 25 49 C 38.234 49 49 38.234 49 25 C 49 11.767 38.234 1 25 1 z M 25.96875 16 C 25.45675 16 24.9535 16.20275 24.5625 16.59375 C 23.7815 17.37475 23.7815 18.62525 24.5625 19.40625 L 28.125 23 L 2.96875 23 C 1.86475 23 0.96875 23.896 0.96875 25 C 0.96875 26.104 1.86475 27 2.96875 27 L 28.125 27 L 24.5625 30.59375 C 23.7815 31.37475 23.7815 32.62525 24.5625 33.40625 C 25.3435 34.18725 26.595 34.18725 27.375 33.40625 L 34.375 26.40625 C 35.156 25.62525 35.156 24.37475 34.375 23.59375 L 27.375 16.59375 C 26.985 16.20275 26.48075 16 25.96875 16 z"/>
                            </svg>
                        </a>';
                }
            ?>

            </div>
        </div>
    </nav>
    <?php
        require 'vendor/autoload.php';
        require 'php/config.php';

        $itemId = $_GET['id'];

        $connection = new MongoDB\Client('mongodb://localhost:27017');
        $collection = $connection->{$db}->StoreItems;
        $cartCollection = $connection->{$db}->UserCart;
        
        $product = $collection->findOne(['_id'=> new MongoDB\BSON\ObjectId("$itemId")]);

        if ($product != null) {
            echo '<section class="text-gray-700 body-font overflow-hidden bg-white">
                    <div class="container px-5 py-24 mx-auto">
                        <div class="lg:w-4/5 mx-auto flex flex-wrap">
                            <img alt="ecommerce" class="lg:w-1/2 w-full object-cover object-center rounded border border-gray-200" src="assets/' . $product['image_path'] . '">
                            <div class="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
                                <h1 class="text-gray-900 text-4xl title-font font-bold mb-1">' . $product['item_name'] . '</h1>
                                <div class="flex mb-4">
                                    <span class="title-font font-medium text-2xl text-gray-900">' . $product['price'] . ' €</span>
                                </div>
                                <p class="leading-relaxed">' . $product['description'] . '</p>
                                <div class="flex border-t border-gray-300 mt-5 pt-5">
                                    <button id="addButton" type="button" onclick="addToCart()" class="bg-gray-800 duration-200 focus:outline-none focus:shadow-outline font-medium h-12 hover:bg-gray-900 inline-flex items-center justify-center px-6 text-white tracking-wide transition w-full">
                                        Add to Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>';
        }
    ?>
    
    <script>
        function addToCart(){
            var addButton = document.getElementById("addButton");
            if (addButton.innerText != 'Added to cart.') {
                $.ajax({
                    type: "POST",
                    url: "/php/cart/add_prod_to_cart.php",
                    data: { userId: <?php echo '"' . $_COOKIE["gochistore_userid"] . '"'; ?>, itemId: <?php echo '"' . $itemId . '"'; ?> }
                }).done(function( msg ) {
                    addButton.classList.remove("bg-gray-800");
                    addButton.classList.remove("hover:bg-gray-900");
                    addButton.classList.add("bg-pink-300");
                    addButton.classList.add("hover:bg-pink-500");
                    addButton.innerText = 'Added to cart.';
                });
            }
        }
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
        function logout(){
            $.ajax({
                    type: "POST",
                    url: "/php/logout.php",
                    data: { }
                }).done(function( msg ) {
                    window.location.reload();
                });
        }
    </script>

</body>
</html>